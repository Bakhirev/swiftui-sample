//
//  CurrenciesListViewModel.swift
//  Terminal
//
//  Created by Bakhirev on 05.05.2021.
//

import Combine
import SwiftUI

class CurrencyListViewModel: ObservableObject {
    @Published var currencies: [Currency] = []
    
    init () {
        currencies = [
            Currency(name: "Bitcoin", value: 10, udsValue: 70001, percentDifference: -2.5, symbol: "btc", holdings: 200000, available: 300000),
            Currency(name: "Etherium", value: 8, udsValue: 102, percentDifference: 0.8, symbol: "eth", holdings: 1000, available: 200),
            Currency(name: "Miota", value: 1, udsValue: 5, percentDifference: 7.5, symbol: "miota", holdings: 2000, available: 200000),
            Currency(name: "USDC", value: 1000, udsValue: 2087, percentDifference: -10.1, symbol: "usdc", holdings: 300000, available: 20000)
        ]
    }
}
