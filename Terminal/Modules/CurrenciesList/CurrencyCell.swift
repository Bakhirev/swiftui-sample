import SwiftUI

struct CurrencyCell: View {
    let currency: Currency
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Image(currency.symbol).padding(.top, 5)
                Text(currency.name)
                    .font(.title2).fontWeight(.bold)
                    .foregroundColor(Color.white)
            }
            
            Text(String(format: "%.2f %@", currency.value, currency.symbol.uppercased()))
                .font(.footnote).fontWeight(.bold)
                .foregroundColor(Color.white)
            
            differenceStack
        }
    }
    
    private var differenceStack: some View {
        HStack {
            Text(String(format: "%.2f USD", currency.udsValue))
                .foregroundColor(Color.gray)
                .font(.footnote)
            Spacer()
            Image(currency.percentDifference > 0 ? "difPlus" : "difMinus")
                .resizable()
                .frame(width: 10, height: 10, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            Text(String(format: "%.2f", currency.percentDifference))
                .foregroundColor(currency.percentDifference > 0 ? Color.green : Color.red)
                .font(.footnote).fontWeight(.bold)
        }
    }
}

struct CurrencyCell_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CurrencyCell(currency: Currency.mock)
        }
    }
}

