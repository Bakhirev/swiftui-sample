//
//  CurrenciesList.swift
//  Terminal
//
//  Created by Bakhirev on 05.05.2021.
//

import SwiftUI

struct CurrenciesList: View {
    @ObservedObject var viewModel: CurrencyListViewModel
    let didSelect: (Currency) -> Void
    
    var body: some View {
        List {
            ForEach(viewModel.currencies) { currency in
                CurrencyCell(currency: currency)
                    .onTapGesture {
                        self.didSelect(currency)
                    }
            }
            .listRowBackground(Color("Main").opacity(0.95))
        }.cornerRadius(10)
        .ignoresSafeArea()
        .onAppear(perform: {
            UITableView.appearance().backgroundColor = UIColor(named: "Main")
        })
    }
}

struct CurrenciesList_Previews: PreviewProvider {
    static var previews: some View {
        CurrenciesList(viewModel: CurrencyListViewModel(), didSelect: { _ in })
    }
}
