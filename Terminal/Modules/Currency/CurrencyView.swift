import SwiftUI

struct CurrencyView: View {
    let currency: Currency
    let didTapShowChart: () -> Void
    
    var columns: [GridItem] = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        ZStack {
            Color("Main").ignoresSafeArea()
            
            VStack {
                LazyVGrid(
                    columns: columns,
                    alignment: .center,
                    spacing: 16,
                    pinnedViews: [.sectionHeaders, .sectionFooters]
                ) {
                    Section(header: Text(currency.name).font(.title)) {
                        Text("Price")
                            .foregroundColor(Color.white)
                        Text(String(format: "%.2f %@", currency.value, currency.symbol.uppercased()))
                            .foregroundColor(Color.white)
                        
                        Text("Holdings")
                            .foregroundColor(Color.white)
                        Text(String(format: "%.2f", currency.holdings))
                            .foregroundColor(Color.white)
                        
                        Text("Available")
                            .foregroundColor(Color.white)
                        Text(String(format: "%.2f", currency.available))
                            .foregroundColor(Color.white)
                    }
                    .foregroundColor(Color.white)
                }
                .padding(10)
                
                Button("Show chart".uppercased(), action: {
                    self.didTapShowChart()
                })
                
            }
        }
    }
}

struct CurrencyView_Previews: PreviewProvider {
    static var previews: some View {
        CurrencyView(currency: Currency.mock, didTapShowChart: {})
    }
}
