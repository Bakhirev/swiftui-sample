//
//  PlotView.swift
//  Terminal
//
//  Created by Bakhirev on 05.05.2021.
//

import Foundation
import SwiftUI

struct ChartView: View {
    var currency: Currency?
    
    var body: some View {
        VStack {
            if let currency = currency {
                Text("\(currency.name) chart")
                    .font(Font.title2.weight(.bold))
                    .foregroundColor(.white)
            }
            
            Image("chart")
                .resizable()
                .aspectRatio(contentMode: .fit)
        }
    }
}

struct ChartView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ChartView(currency: Currency.mock)
        }
    }
}
