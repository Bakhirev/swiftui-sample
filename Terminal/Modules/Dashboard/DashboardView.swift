//
//  ContentView.swift
//  Terminal
//
//  Created by Bakhirev on 05.05.2021.
//

import SwiftUI

struct DashboardView: View {
    @State private var isShowCurrencyFlow = false
    @State private var selectedCurrency: Currency = Currency.mock
    
    var body: some View {
        NavigationView {
            ZStack {
                Color("Main").ignoresSafeArea()
                VStack {
                    InfoView()
                        .padding(.horizontal, 20)

                    CurrenciesList(viewModel: CurrencyListViewModel(), didSelect: { currency in
                        self.selectedCurrency = currency
                        self.isShowCurrencyFlow = true
                    })
                    
                    NavigationLink(
                        destination: CurrencyCoordinator(currency: selectedCurrency),
                        isActive: $isShowCurrencyFlow
                    ) { EmptyView() }
                }
            }
            .navigationBarHidden(true)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            DashboardView()
            DashboardView()
                .preferredColorScheme(.light)
                .previewDevice("iPad (8th generation)")
        }
    }
}
