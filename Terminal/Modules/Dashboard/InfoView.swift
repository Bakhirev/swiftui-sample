//
//  InfoView.swift
//  Terminal
//
//  Created by Bakhirev on 05.05.2021.
//

import SwiftUI

struct InfoView: View {
    @Environment(\.horizontalSizeClass) var horizontalSizeClass
    
    var body: some View {
        if horizontalSizeClass == .compact {
            compactLayoutView
        } else {
            regularLayoutView
        }
    }
    
    private var compactLayoutView: some View {
        VStack(alignment: .leading) {
            ChartView()
            HStack {
                titleLabel
                Spacer()
                showInBTCButton
            }
            
            amountStack
        }
        .background(Color("Main"))
    }
    
    private var regularLayoutView: some View {
        HStack {
            VStack(alignment: .leading, spacing: 5) {
                titleLabel
                amountStack
                HStack {
                    showInBTCButton
                }
            }
            Spacer()
            ChartView()
        }
        .background(Color("Main"))
    }
    
    private var titleLabel: some View {
        Text("Portfolio value")
            .font(Font.title2.weight(.bold))
            .foregroundColor(Color.white)
    }
    
    private var amountStack: some View {
        HStack(alignment: .bottom, spacing: 2) {
            amountLabel
            curryncySymbolLabel
        }
    }
    
    private var amountLabel: some View {
        Text("123.456.789")
            .font(Font.largeTitle.weight(.bold))
            .foregroundColor(Color.white)
    }
    
    private var curryncySymbolLabel: some View {
        Text("USD")
            .font(Font.title.weight(.bold))
            .foregroundColor(Color.gray)
    }
    
    private var showInBTCButton: some View {
        Button(action: {
            
        }, label: {
            Text("Show in BTC")
                .padding(.horizontal, 10)
                .padding(.vertical, 5)
                .background(
                    RoundedRectangle(cornerRadius: 20)
                        .stroke(lineWidth: 2)
                )
                .accentColor(Color.orange)
        })
    }
}

struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            InfoView()
                .previewDevice("iPhone 12")
            InfoView()
                .previewDevice("iPad (8th generation)")
        }
    }
}
