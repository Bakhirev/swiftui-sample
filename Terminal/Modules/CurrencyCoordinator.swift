import SwiftUI

struct CurrencyCoordinator: View {
    enum Route: String {
        case info
        case chart
    }
    
    @State var currency: Currency
    @State private var selection: Route? = .info
    
    var body: some View {
        VStack {
            CurrencyView(currency: currency, didTapShowChart: {
                self.selection = Route.chart
            })
            
            NavigationLink(
                destination: ChartView(currency: currency),
                tag: Route.chart,
                selection: $selection
            ) { EmptyView() }
        }
    }
}

struct CoordinatorView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CurrencyCoordinator(currency: Currency.mock)
        }
    }
}
