//
//  TerminalApp.swift
//  Terminal
//
//  Created by Bakhirev on 05.05.2021.
//

import SwiftUI

@main
struct TerminalApp: App {
    var body: some Scene {
        WindowGroup {
            DashboardView()
        }
    }
}
