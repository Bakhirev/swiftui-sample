//
//  Currency.swift
//  Terminal
//
//  Created by Bakhirev on 05.05.2021.
//

import Foundation

struct Currency: Identifiable {
    var id = UUID()
    let name: String
    let value: Double
    let udsValue: Double
    let percentDifference: Double
    let symbol: String
    let holdings: Double
    let available: Double
    
    static var mock = Currency(
        name: "Bitcoin",
        value: 108,
        udsValue: 10002,
        percentDifference: 2.5,
        symbol: "btc",
        holdings: 2000000,
        available: 3000000
    )
}
